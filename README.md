# SysVInit script for setting s2idle as suspend method for Pinebook Pro

Currently in mainline Linux kernel deep sleep support for Pinebook Pro is unreliable.
Causes system to put into deep sleep mode but does never wake up. This script
sets suspend method to `s2idle` and therefore suspend works. However this is not
best a solution because CPUs are not disabled in this mode. It only freezes whole userspace
and puts CPUs in deep idle mode what results of course in lowered power consumption
but not that low as in deep sleep mode.

## Installation

Copy script `s2idle-set` to `/etc/init.d` and then execute as root:

```
update-rc.d s2idle-set defaults
```

This will put `s2idle-set` service in boot order. This script runs only during S runlevel
during system boot.

## Usage

After step above use:

```
service s2idle-set start
```

or

```
/etc/init.d/s2idle-set start
```

to set sleep mode to `s2idle`